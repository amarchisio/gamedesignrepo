﻿using System.Collections;

using System.Collections.Generic;

using UnityEngine;



[System.Serializable]

public class Boundary

{

    //initializes variables for the boundary that can be adjusted through unity

    public float xMin, xMax, yMin, yMax;

}



public class PlayerController : MonoBehaviour

{

    //initializes variables for the boundary that can be adjusted through unity

    public float speed;
    public float tilt;
    public Boundary boundary;
    public float zMax, zMin;


    void OnTriggerEnter(Collider other)

    {


        if (other.tag == "stop")
        {
            zMax = 300;

        }
        if (other.tag == "start")
        {
            zMax = 1000;

        }
        if (other.tag == "stop2")
        {
            zMax = 620;

        }

    }

        void FixedUpdate()

    {

        float moveHorizontal = Input.GetAxis("Horizontal");

        float moveVertical = Input.GetAxis("Vertical");


        //grabs values from moveHorizontal and moveVertical to move along the x and y axis

        Vector3 movement = new Vector3(moveHorizontal, moveVertical, 1);

        //speed of ship

        GetComponent<Rigidbody>().velocity = movement * speed;



        //increasing or decreasing speed using E and Q keys

        if (Input.GetKeyDown(KeyCode.E))
        {

            //cap on minimum speed

            if (speed <= 4)
            {

                return;

            }

            else speed = speed - 2;

        }

        if (Input.GetKeyDown(KeyCode.Q))
        {

            //cap on maximum speed

            if (speed >= 12)
            {

                return;

            }

            else speed = speed + 2;

        }



        //limits the ship to the boundary given by the user

        GetComponent<Rigidbody>().position = new Vector3

             (

             Mathf.Clamp(GetComponent<Rigidbody>().position.x, boundary.xMin, boundary.xMax),

             Mathf.Clamp(GetComponent<Rigidbody>().position.y, boundary.yMin, boundary.yMax),

             Mathf.Clamp(GetComponent<Rigidbody>().position.z, zMin, zMax)

             );



        //titls the ship depending on whether you're moving vertically or horixzontally

        GetComponent<Rigidbody>().rotation = Quaternion.Euler(GetComponent<Rigidbody>().velocity.y * -tilt, 0.0f, GetComponent<Rigidbody>().velocity.x * -tilt);

    }



}