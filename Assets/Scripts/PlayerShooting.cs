﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShooting : MonoBehaviour {

	public float coolDown = 0f;
	public float fireRate = 0f;
	public float PowerUpTime;
	private bool moreGunsPowUp = false;

	//check to see if we're actually firing
	public bool isFiring = false;

	//firing point transforms for launching projectiles
	public Transform leftFirePoint;
	public Transform rightFirePoint;
	public Transform leftPowerUpFirePoint;
	public Transform rightPowerUpFirePoint;

	//our projectile object
	public GameObject laserPrefab;

	public AudioSource fireFXsound;


	void Start () {

	isFiring = false;
	

	}
	
	// Update is called once per frame
	void Update () {

		CheckInput ();
		coolDown -= Time.deltaTime;

		if (isFiring == true) 
		{
			//the player has initiated the laser
			Fire();

		}


		
	}
	void CheckInput(){
		
		if ( Input.GetKeyDown ("space"))
		{
			isFiring = true;
		}
		else {
			isFiring = false;
		}
}

	void Fire()
	{
		if (coolDown > 0) {
			return; //do not fire

		}
		//Player sound FX when player is firing
		if (fireFXsound != null) { 
			fireFXsound.Play ();


		}

		//adds extra guns if PowUp = true
		if (moreGunsPowUp == false) {
			GameObject.Instantiate (laserPrefab, leftFirePoint.position, leftFirePoint.rotation);
			GameObject.Instantiate (laserPrefab, rightFirePoint.position, rightFirePoint.rotation);
		} 
		else {
			GameObject.Instantiate (laserPrefab, leftFirePoint.position, leftFirePoint.rotation);
			GameObject.Instantiate (laserPrefab, rightFirePoint.position, rightFirePoint.rotation);
			GameObject.Instantiate (laserPrefab, leftPowerUpFirePoint.position, leftPowerUpFirePoint.rotation);
			GameObject.Instantiate (laserPrefab, rightPowerUpFirePoint.position, rightPowerUpFirePoint.rotation);
		}

			
		coolDown = fireRate;

	}

	//rate of fire power-up
	IEnumerator OnTriggerEnter(Collider other)
	{
		//if player runs into a powerup
		if (other.tag == "PowerUp") {
			
			//fire rate is doubled
			fireRate = fireRate / 2;

			//after PowerUpTime seconds, the power-up ends
			yield return new WaitForSeconds(PowerUpTime);
			fireRate = fireRate * 2; 
		} 
		if (other.tag == "PowerUp2") {

			//powerup active
			moreGunsPowUp = true;
			//after PowerUpTime seconds, the power-up ends
			yield return new WaitForSeconds (PowerUpTime);
			//deactivate power-up
			moreGunsPowUp = false;
		}
	} 
}