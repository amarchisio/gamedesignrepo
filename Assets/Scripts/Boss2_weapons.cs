﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss2_weapons : MonoBehaviour
{

    public GameObject shot;
    public Transform W;
    public Transform W1;
    public Transform W2;
    public Transform W3;
    public Transform W4;
    public Transform W5;
    public Transform W6;

    public float fireRate;
    public float delay;

    private AudioSource audioSource;

    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        InvokeRepeating("Fire", delay, fireRate);
    }

    void Fire()
    {
        Instantiate(shot, W.position, W.rotation);
        Instantiate(shot, W1.position, W1.rotation);
        Instantiate(shot, W2.position, W2.rotation);
        Instantiate(shot, W3.position, W3.rotation);
        Instantiate(shot, W4.position, W4.rotation);
        Instantiate(shot, W5.position, W5.rotation);
        Instantiate(shot, W6.position, W6.rotation);

        audioSource.Play();
    }
}