﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MoveTowards : MonoBehaviour {
	private Transform target;
	private Rigidbody rb;

	public float speed;
	public float tilt;

	GameObject player;
	Rigidbody playerBody;
	Vector3 playerPos;
	Rigidbody enemyBody;
	Vector3 enemyPos;

	void Start() {
		target = GameObject.Find ("Player").transform;
		player = GameObject.Find ("Player");
		rb = GetComponent<Rigidbody>();
		playerBody = player.GetComponent<Rigidbody>();
		enemyBody = this.GetComponent<Rigidbody> (); 
	}


	void Update() {
		playerPos = playerBody.position;
		enemyPos = enemyBody.position;

		float step = speed * Time.deltaTime;
		transform.position = Vector3.MoveTowards(transform.position, target.position, step);
		if (enemyPos.z < playerPos.z) {
			Destroy (gameObject);
		}
		//transform.LookAt(target.position);
	}

	void FixedUpdate() {
		rb.rotation = Quaternion.Euler(rb.velocity.y * -tilt, 0.0f, rb.velocity.x * -tilt);
	}
}