﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class deactivatesheild : MonoBehaviour {

    public GameObject explosion;
    public GameObject playerExplosion;
    public GameObject shield;
    private object gameController;

    void OnTriggerEnter(Collider other)
    {

        if (other.tag == "hazard")
        {
            Instantiate(playerExplosion, other.transform.position, other.transform.rotation);
            Destroy(other.gameObject);
            shield.SetActive(false);
        }
       
    }
}