﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AI_Control : MonoBehaviour {

	GameObject player;
	Rigidbody playerBody;
	Vector3 playerPos;
	Rigidbody enemyBody;
	Vector3 enemyPos;

	public float speed;
	public float tilt;

	void Start () {

		player = GameObject.Find ("Player");
		playerBody = player.GetComponent<Rigidbody>();
		enemyBody = this.GetComponent<Rigidbody> (); 

	}

	// Update is called once per frame
	void Update () {

		Vector3 newPos = new Vector3 ();
		playerPos = playerBody.position;
		enemyPos = enemyBody.position;

		float differenceX = enemyPos.x - playerPos.x;
		float differenceY = enemyPos.y - playerPos.y;

		newPos.Set (player.transform.position.x, player.transform.position.y, transform.position.z + 1.0f); 
		//newPos.Set ((enemyPos.x + (1.0f / 5.0f * differenceX)), enemyPos.y + (1.0f / 5.0f * differenceY), enemyPos.z);

		//Debug.Log (differenceX + ", " + differenceY);
		//Debug.Log ("Player Pos: [" + playerPos.x + "," + playerPos.y + "]\tEnemy Pos: [" + enemyPos.x + "," + enemyPos.y + "]New Pos: [" + newPos.x + "," + newPos.y + "]" );
		Debug.Log ("Enemy velocity y = " + enemyBody.velocity.y + "Enemy Velocity x = " + enemyBody.velocity.x);

		transform.position = Vector3.MoveTowards(transform.position, newPos, speed * Time.deltaTime);
	}
	void FixedUpdate (){
		enemyBody.rotation = Quaternion.Euler(enemyBody.velocity.y * -tilt, 0.0f, enemyBody.velocity.x * -tilt);
		//rotate towards
	}
}	
