﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class boss1_weapons : MonoBehaviour
{

    public GameObject shot;
    public Transform right;
    public Transform left;
    public Transform middle;
    public float fireRate;
    public float delay;

    private AudioSource audioSource;

    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        InvokeRepeating("Fire", delay, fireRate);
    }

    void Fire()
    {
        Instantiate(shot, right.position, right.rotation);
        Instantiate(shot, left.position, left.rotation);
        Instantiate(shot, middle.position, middle.rotation);
        audioSource.Play();
    }
}