﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyByContactboss : MonoBehaviour
{
    public GameObject explosion;
    public GameObject playerExplosion;
    private object gameController;
    public GameObject stop;
    public GameObject start;

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Boundary")
        {
            return;
        }
        Instantiate(explosion, transform.position, transform.rotation);
        if (other.tag == "Player")
        {
            stop.SetActive(false);
            start.SetActive(true);
            Instantiate(playerExplosion, other.transform.position, other.transform.rotation);
        }
        Destroy(other.gameObject);
        Destroy(gameObject);
    }
}