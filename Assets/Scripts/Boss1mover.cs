﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss1mover : MonoBehaviour
{
	public float speed;

    void Start()
    {
        GetComponent<Rigidbody>().velocity = transform.right * speed;
    }
}
