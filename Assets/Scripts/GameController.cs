﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    public GameObject hazard;
    public Vector3 spawnValuesmax;
    public Vector3 spawnValuesmin;
    public int hazardCount;
    public float spawnWait;
    public float startWait;
    public float waveWait;
    public GameObject player;

 
    void OnTriggerEnter(Collider other)
    {
        
        if (other.tag == "endspawn")
        {
            hazard.SetActive(false);
        
        }

        if (other.tag == "startspawn")
        {
            hazard.SetActive(true);

        }
     
    }
        void Start()
    {
        StartCoroutine(SpawnWaves());

    }

    IEnumerator SpawnWaves()
    {
        yield return new WaitForSeconds(startWait);
        while (true)
        {
            for (int i = 0; i < hazardCount; i++)
            {
                    Vector3 spawnPosition = new Vector3(Random.Range(spawnValuesmin.x, spawnValuesmax.x), Random.Range(spawnValuesmin.y, spawnValuesmax.y), Random.Range(spawnValuesmin.z + player.transform.position.z, spawnValuesmax.z));
                Quaternion spawnRotation = Quaternion.identity;
                        Instantiate(hazard, spawnPosition, spawnRotation);

            }
            yield return new WaitForSeconds(waveWait);
        }
    }
}